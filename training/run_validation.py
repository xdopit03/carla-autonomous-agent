#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of training.
#
# It contains functions for validation models for carla leaderboard team code. 
#
# Author:	Marek Dopita (xdopit03)
#

import torch
import os
import getopt as go
import sys
import matplotlib.pyplot as plt

from datetime import datetime
from utilities import ArgumentParsingException, UndefinedException
from utilities import print_error, log, progres_print
from neural_network import Model, Loss
from datasets import Dataset

def execute_validation(model, load_model, dataset, path):
	"""Function execute validation of neural network model.
	
	Parameters
	----------
	model : str
		Name of model type for validation.
	load_model: str
		Name of file with already trained model for loading and validation.
	dataset : str
		Name of dataset to use.
	path : str
		Path to dataset.
		
	Returns
	-------
	err_code : int
		Number of err code, 0 when success.
	"""
	
	try:
		
		# if dastaset path not exists
		if not os.path.exists(path):
			return 7
		
		# prepare dataset
		dataset = Dataset(path, dataset)
		
		# init dataset data loader
		data_loader = torch.utils.data.DataLoader(dataset, shuffle=False)
		
		# create model
		model = Model(model)
		
		# load saved model state
		loaded_state = torch.load(os.path.join(os.environ["TEAM_CODE_ROOT"], "trained_models/", load_model))
		
		# load model from save
		model.load_state_dict(loaded_state['model'])
		
		# set evaluation for model
		model.eval()
		
		# sum loss
		loss_function = Loss('mean-squared-error')
		loss_sum = 0.0
		
		# dataset size
		max_count = len(data_loader)
		counter = 0
		
		# list of plt nodes
		x = []
		t_t = []
		t_s = []
		t_b = []
		o_t = []
		o_s = []
		o_b = []
		
		# foreach data
		for data in data_loader:
			
			# print progress
			counter += 1
			progres_print(counter, max_count, os.get_terminal_size()[0] - 10)
			
			# squeeze data from dataloader
			for key in data.keys():
				data[key] = torch.squeeze(data[key], 0)
			
			# compute output
			output = model(data)
			
			# craete target
			target = torch.squeeze(data['control'])
			
			# append results to lists
			x.append(counter)
			t_t.append(target[0].item())
			o_t.append(output[0].item())
			t_b.append(target[1].item())
			o_b.append(output[1].item())
			t_s.append(target[2].item())
			o_s.append(output[2].item())
			
			# compute loss
			loss = loss_function(output, target)
			loss_sum += loss.item()
		
		# print results and targets
		plt.figure(figsize=(18, 12))
		plt.subplot(2, 2, 1)
		plt.title('Plyn')
		plt.scatter(x, t_t, color= "green", s=30, label="Očekávané hodnoty")
		plt.scatter(x, o_t , color= "red", s=30, label="Získané hodnoty")
		plt.legend()
		
		plt.subplot(2, 2, 2)
		plt.title('Brzda')
		plt.scatter(x, t_b, color= "green", s=30, label="Očekávané hodnoty")
		plt.scatter(x, o_b, color= "red", s=30, label="Získané hodnoty")
		plt.legend()
		
		plt.subplot(2, 2, 3)
		plt.title('Natočení kol')
		plt.scatter(x, t_s, color= "green", s=30, label="Očekávané hodnoty")
		plt.scatter(x, o_s, color= "red", s=30, label="Získané hodnoty")
		plt.legend()
		
		# save image
		plt.savefig(os.path.join(os.environ["TEAM_CODE_ROOT"], "trained_models/validation_result-" \
			+ datetime.now().strftime("%d_%m_%Y-%H:%M:%S") + ".jpg"))
		
		# log validation results
		log('Validate with loss: ' + str(loss_sum))
		
	# handle exceptions
	except UndefinedException as e:
		print_error('Undefined: ' + str(e) + '.')
		return 5
	except FileNotFoundError:
		print_error('File for load neural network state not found.')
		return 6
	except KeyboardInterrupt:
		print_error('Keybord interrupt.')

def print_help():
	"""
	Print help for script.
	"""
	
	print("""Run validation for neural network models created for CARLA leaderboard team code.
	
Usage: 
	run_validation [-l model_name] [-h] -d dataset_name -m model_name

Arguments:
	-l, --load_model NAME	Name of loaded model file.
	-h, --help		Print help.
	-d, --dataset NAME	Dataset name used for validation.
	-m, --model NAME	Name of model type to validate.
	-p, --dataset_path PATH	Path to training dataset.
	
Exit status:
	0 if OK,
	1 if argument parsing error,
	2 if missing model type,
	3 if load file name missing,
	4 if dataset type name missing,
	5 if dataset or model type is undefined,
	6 if file for load not found.
	7 if dataset path not found.""")

# program access point
if __name__ == '__main__':
	
	# variables for set
	model = ''
	load_model = ''
	dataset = ''
	help = False
	path = ''
	
	# handle arguments
	try:
		opts, args = go.getopt(sys.argv[1:], 'l:hd:m:p:', ['load_model=', 'help', 'dataset=', 'model=', 'dataset_path='])
		
		# if program has arguments without options
		if len(args) > 0:
			raise ArgumentParsingException('unrecognized arguments - ' + str(args))
		
		# foreach options
		for opt, arg in opts:
		
			# option for model load
			if opt in ['-l', '--load_model']:
				if load_model != '': raise ArgumentParsingException('option ' + opt + ' has double definition')
				load_model = arg
				
			# option for help
			elif opt in ['-h', '--help']:
				help = True
				
			# option for dataset
			elif opt in ['-d', '--dataset']:
				if dataset != '': raise ArgumentParsingException('option ' + opt + ' has double definition')
				dataset = arg
			
			# option for model
			elif opt in ['-m', '--model']:
				if model != '': raise ArgumentParsingException('option ' + opt + ' has double definition')
				model = arg
				
			# option for dastaset path
			elif opt in ['-p', '--dataset_path']:
				if path != '': raise ArgumentParsingException('option ' + opt + ' has double definition')
				path = arg
	
	# handle exceptions
	except go.GetoptError as e:
		print_error('Wrong argument: ' + str(e))
		sys.exit(1)
	except ArgumentParsingException as e:
		print_error('Wrong argument: ' + str(e) + '.')
		sys.exit(1)
	
		# print help or run training
	if help:
		print_help()
	else:
		# if nn model not setted
		if model == '':
			print_error('Neural network model missing.')
			sys.exit(2)
		
		# if file name for saving missing
		if load_model == '':
			print_error('File name for loading trained neural network missing.')
			sys.exit(3)
		
		# if dataset specification missing
		if dataset == '':
			print_error('Dataset name missing.')
			sys.exit(4)
		
		# if dataset path missing
		if path == '':
			print_error('Dataset path missing.')
			sys.exit(7)
		
		# execute
		err_code = execute_validation(model, load_model, dataset, path)
		
		# if error exit with given number
		if err_code != 0:
			sys.exit(err_code)
