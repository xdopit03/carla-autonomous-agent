#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of training.
#
# It contains functions for training models for carla leaderboard team code. 
#
# Author:	Marek Dopita (xdopit03)
#

import torch
import os
import torch.optim as optim
import numpy as np
import getopt as go
import sys

from utilities import ArgumentParsingException, UndefinedException
from utilities import print_error, log, progres_print
from neural_network import Model, Loss
from datasets import Dataset

def execute_training(model, save_model, load_model, dataset, path):
	"""Function execute training of neural network model.
	
	Parameters
	----------
	model : str
		Name of model type for training.
	save_model : str
		Name of file to save trained model.
	load_model: str
		Name of file with already trained model for loading and training continue.
	dataset : str
		Name of dataset to use.
	path : str
		Path to dataset.
		
	Returns
	-------
	err_code : int
		Number of err code, 0 when success.
	"""
	
	try:
		
		# if dastaset path not exists
		if not os.path.exists(path):
			return 7
		
		# prepare dataset
		dataset = Dataset(path, dataset)
		
		if len(dataset) == 0:
			print_error('Datast has no records.')
			return 8
		
		# init dataset data loader
		data_loader = torch.utils.data.DataLoader(dataset, shuffle=True,  batch_size=1)
		
		# create model
		model = Model(model)
		
		# when loading file
		if load_model != '':
		
			# path into trained models
			loaded_state = torch.load(os.path.join(os.environ['TEAM_CODE_ROOT'], 'trained_models/', load_model))
		
			# load model from save
			model.load_state_dict(loaded_state['model'])
		
			# set iteration from save
			iteration = loaded_state['iteration']
		else:
			# init default
			loaded_state = None
			iteration = 0
			
		# set optimizer
		optimizer = optim.Adam(model.parameters())
			
		# when loaded state
		if load_model != '':
			
			# load optimizer state
			optimizer.load_state_dict(loaded_state['optimizer'])
		
		# get loss function
		loss_function = Loss('mean-squared-error')
		loss_sum = 0.0
		
		# dataset size
		max_count = len(data_loader)
		counter = 0
		
		# foreach data
		for data in data_loader:
			
			# print progress
			counter += 1
			progres_print(counter, max_count, os.get_terminal_size()[0] - 10)
			
			# increment iteration
			iteration += 1
			
			# clear previous gradient
			model.zero_grad()
			
			# squeeze data from dataloader
			for key in data.keys():
				data[key] = torch.squeeze(data[key], 0)
			
			# forward move
			output = model(data)
			
			# craete target
			target = torch.squeeze(data['control'])
			
			# compute loss
			loss = loss_function(output, target)
			
			# compute gradient
			loss.backward()
			
			# update parameters based on gradient
			optimizer.step()
			
			loss_sum += loss.item()
		
		log('\nTrained with loss - sum: ' + str(loss_sum) + ', avg: ' + str(loss_sum/max_count))
		
		# save model
		state = {
			'iteration' : iteration,
			'model' : model.state_dict(),
			'optimizer' : optimizer.state_dict()
		}
		
		# save file
		model_name = save_model + '.pt'
		model_path = os.path.join(os.environ['TEAM_CODE_ROOT'], 'trained_models/', model_name)
		torch.save(state, model_path)
		
	# handle exceptions
	except UndefinedException as e:
		print_error('Undefined: ' + str(e) + '.')
		return 5
	except FileNotFoundError:
		print_error('File for load neural network state not found.')
		return 6
	except KeyboardInterrupt:
		print_error('Keybord interrupt.')
	
	return 0

def print_help():
	"""
	Print help for script.
	"""
	
	print("""Run training for neural network models created for CARLA leaderboard team code.
	
Usage: 
	run_training [-l model_name] -s model_name [-h] -d dataset_name -m model_name

Arguments:
	-l, --load_model NAME	Name of loaded model file.
	-s, --save_model NAME	Name of file for save.
	-h, --help		Print help.
	-d, --dataset NAME	Dataset name used for training.
	-m, --model NAME	Name of model type to train.
	-p, --dataset_path PATH	Path to training dataset.
	
Exit status:
	0 if OK,
	1 if argument parsing error,
	2 if missing model type,
	3 if save file name missing,
	4 if dataset type name missing,
	5 if dataset or model type is undefined,
	6 if file for load not found.
	7 if dataset path not found.
	8 if dataset empty.""")

# program access point
if __name__ == '__main__':
	
	# variables for set
	model = ''
	help = False
	load_model = ''
	save_model = ''
	dataset = ''
	path = ''
	
	# handle arguments
	try:
		opts, args = go.getopt(sys.argv[1:], 'l:s:hd:m:p:', ['load_model=', 'save_model=', 'help', 'dataset=', 'model=', 'dataset_path='])
		
		# if program has arguments without options
		if len(args) > 0:
			raise ArgumentParsingException('unrecognized arguments - ' + str(args))
		
		# foreach options
		for opt, arg in opts:
		
			# option for model load
			if opt in ['-l', '--load_model']:
				if load_model != '': raise ArgumentParsingException('option ' + opt + ' has double definition')
				load_model = arg
			
			# option for model save
			elif opt in ['-s', '--save_model']:
				if save_model != '': raise ArgumentParsingException('option ' + opt + ' has double definition')
				save_model = arg
				
			# option for help
			elif opt in ['-h', '--help']:
				help = True
				
			# option for dataset
			elif opt in ['-d', '--dataset']:
				if dataset != '': raise ArgumentParsingException('option ' + opt + ' has double definition')
				dataset = arg
			
			# option for model
			elif opt in ['-m', '--model']:
				if model != '': raise ArgumentParsingException('option ' + opt + ' has double definition')
				model = arg
			
			# option for dastaset path
			elif opt in ['-p', '--dataset_path']:
				if path != '': raise ArgumentParsingException('option ' + opt + ' has double definition')
				path = arg
	
	# handle exceptions
	except go.GetoptError as e:
		print_error('Wrong argument: ' + str(e))
		sys.exit(1)
	except ArgumentParsingException as e:
		print_error('Wrong argument: ' + str(e) + '.')
		sys.exit(1)
	
	# print help or run training
	if help:
		print_help()
	else:
		# if nn model not setted
		if model == '':
			print_error('Neural network model missing.')
			sys.exit(2)
		
		# if file name for saving missing
		if save_model == '':
			print_error('File name for saving trained neural network missing.')
			sys.exit(3)
		
		# if dataset specification missing
		if dataset == '':
			print_error('Dataset name missing.')
			sys.exit(4)
		
		# if dataset path missing
		if path == '':
			print_error('Dataset path missing.')
			sys.exit(7)
	
		# execute
		err_code = execute_training(model, save_model, load_model, dataset, path)
		
		# if error exit with given number
		if err_code != 0:
			sys.exit(err_code)
