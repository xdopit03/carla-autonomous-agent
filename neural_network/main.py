#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of neural_network.
#
# It contains functions to select models and loss functions. 
#
# Author:	Marek Dopita (xdopit03)
#

import torch.nn

from .nn_model_xdopit03_cl import NNModelXdopit03CL
from .nn_model_xdopit03_ju import NNModelXdopit03JU
from .nn_model_xdopit03_lf import NNModelXdopit03LF
from utilities import UndefinedException

def Model(model_type_name):
	"""Return neural network model class by selected model name.
	
	Parameters
	----------
	model_type_name : str
		Name of model type.
		
	Raises
	------
	UndefinedException
		If model type name does not exist.
	
	Returns
	-------
	Model : nn.Module
		Class derived from neural network module.
	"""
	
	if model_type_name == "model_xdopit03_cl":
		return NNModelXdopit03CL()
	elif model_type_name == "model_xdopit03_ju":
		return NNModelXdopit03JU()
	elif model_type_name == "model_xdopit03_lf":
		return NNModelXdopit03LF()
	else:
		raise UndefinedException('Udefined model name "' + model_type_name + '".\nPossible model types: model_xdopit03_cl, model_xdopit03_ju, model_xdopit03_lf')
		
def Loss(loss_function_type_name):
	"""Return loss function by function name.
	
	Parameters
	----------
	loss_function_type_name : str
		Name of loss function type.
		
	Raises
	------
	UndefinedException
		If loss function type name does not exist.
	
	Returns
	-------
	LossFunction : nn.LossFunction
		Class derived from neural network loss function.
	"""
	
	if loss_function_type_name == "mean-absolute-error":
		return torch.nn.L1Loss()
	elif loss_function_type_name == "mean-squared-error":
		return torch.nn.MSELoss()
	else:
		raise UndefinedException('Udefined loss function name "' + loss_function_type_name + '".\nPossible function types: mean-absolute-error, mean-squared-error')
