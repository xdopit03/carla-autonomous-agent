#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of neural_network.
#
# It contains class for neural network convolutional part. 
#
# Author:	Marek Dopita (xdopit03)
#

import torch.nn as nn
import torch

from utilities import NetException

class NNFullyConnPart(nn.Module):
	"""Part of neural network model with fully connected layers.
	
	Attributes
	----------
	_layers : nn.Sequential
		Sequential of neural network layers.
	
	Methods
	-------
	forward(data)
		Forward branch of neural network.
	"""
	
	def __init__(self, features, dropout_probability, last_part = False):
		"""Initialization of neural network part.
		
		Parameters
		----------
		features : list
			List of features (neurons) for linear layers as in and out neurons.
		dropout_probability : list
			List of dropout probability.
		"""
		
		# init parent
		super().__init__()
		
		# if invalid inputs
		if len(features) < 2:
			raise NetException("Invalid initialization of neural network part!")
		
		if len(features) - 1 != len(dropout_probability):
			raise NetException("Invalid initialization of neural network part!")
		
		# list of layers
		self._layers = []
		
		# foreach layer params
		for i in range(0, len(features) - 1):
			
			# linear layer
			lin = nn.Linear(
				in_features = features[i],
				out_features = features[i+1]
			)
			
			# random dropout layer
			dropout = nn.Dropout(p = dropout_probability[i])
			
			# relu activation function
			relu = nn.ReLU(inplace = True)
			
			# create layer - if last layer and last part
			if i == len(features) - 2 and last_part:
				layer = nn.Sequential(*[lin, dropout])
			else:
				layer = nn.Sequential(*[lin, dropout, relu])
			
			# append to layers
			self._layers.append(layer)
			
		# sequential pass of layers
		self._layers = nn.Sequential(*self._layers)
		
		
	def forward(self, data):
		"""Forward branch for neural network.
		
		Parameters
		----------
		data : torch.Tensor
			Data from sensors. 
		
		Returns
		-------
		array : torch.Tensor
			Tensor with neural network part output.
		"""
		
		# get output of convolutional part
		output = self._layers(data)
		
		return output
