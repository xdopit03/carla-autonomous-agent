#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of neural_network.
#
# It contains class for neural network convolutional part. 
#
# Author:	Marek Dopita (xdopit03)
#

import torch.nn as nn
import torch

from utilities import NetException

class NNConvolutinalPart(nn.Module):
	"""Part of neural network model with convolutional layers.
	
	Attributes
	----------
	_layers : nn.Sequential
		Sequential of neural network layers.
	
	Methods
	-------
	forward(data)
		Forward branch of neural network
	_number_of_pixels(data)
		Returns number of pixels of output.
	simulate_output_size(size)
		Simulate neural network to get output size.
	"""
	
	def __init__(self, channels, kernel, stride, dropout_probability):
		"""Initialization of neural network part.
		
		Parameters
		----------
		channels : list
			List of channels for conv2d layers as in and out channels.
		kernel : list
			List of kernel sizes for conv2d layers.
		stride : list
			List of stride sizes for conv2d layers.
		dropout_probability : list
			List of dropout probability.
		"""
		
		# init parent
		super().__init__()
		
		# len of channels
		lc = len(channels)
		
		# if invalid inputs
		if lc < 2 or not kernel or not stride or not dropout_probability:
			raise NetException("Invalid initialization of neural network part!")
		
		# if invalid sizes
		if lc - 1 != len(kernel) or lc - 1 != len(stride) or lc - 1 != len(dropout_probability):
			raise NetException("Invalid initialization of neural network part!")
		
		# list of layers
		self._layers = []
		
		# foreach layer params
		for i in range(0, len(channels) - 1):
			
			# create convolutional layer
			conv = nn.Conv2d(
				in_channels = channels[i],
				out_channels = channels[i + 1],
				kernel_size = kernel[i],
				stride = stride[i]
			)
			
			# random dropout layer
			dropout = nn.Dropout(p = dropout_probability[i])
			
			# relu activation function
			relu = nn.ReLU(inplace = True)
			
			# create barch normalization
			batch_norm = nn.BatchNorm2d(num_features = channels[i + 1])
			
			# create layer as sequntail pass
			layer = nn.Sequential(*[conv, dropout, relu, batch_norm])
			
			# append to layers
			self._layers.append(layer)
			
		# sequential pass of layers
		self._layers = nn.Sequential(*self._layers)
		
		
	def forward(self, data):
		"""Forward branch for neural network.
		
		Parameters
		----------
		data : torch.Tensor
			Data from sensors. 
		
		Returns
		-------
		array : torch.Tensor
			Tensor with neural network part output.
		"""
		
		# get output of convolutional part
		output = self._layers(data)
		
		# flatten output for pass to fully connected part
		flattened_output = output.view(-1, self._number_of_pixels(output))
		
		return flattened_output
		
	def _number_of_pixels(self, data):
		"""Get number of image pixels.
		
		Parameters
		----------
		data : torch.Tensor
			Data (image) from convolutional layers.
		
		Returns
		-------
		num_pixels : int
			Number of image pixels.
		"""
		
		# get size of picture without batch dimension
		size = data.shape[1:]
		
		# number of pixels
		num_pixels = 1
		
		# foreach dimension get pixel number
		for dimension in size:
			num_pixels *= dimension
		
		return num_pixels
		
	def simulate_output_size(self, size):
		"""Simulate forward pass of neural network to get output size.
		
		Parameters
		----------
		size : list
			List as image size.
		
		Returns
		-------
		out_features : int
			Returns number of output features (neurons).
		"""
		
		# add dimension
		size.insert(0, 1)
		size = tuple(size)
		
		# create input
		generate_input = torch.zeros(size)
		
		# forward pass
		output = self._layers(generate_input)
		
		# get number of features
		output_features = self._number_of_pixels(output)
		
		return output_features
