#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of neural_network.
#
# It contains class for neural network model. 
#
# Author:	Marek Dopita (xdopit03)
#

import torch.nn as nn
import torch

from .nn_convolutional_part import NNConvolutinalPart
from .nn_fully_conn_part import NNFullyConnPart

class NNModelXdopit03CL(nn.Module):
	"""Model of neural network for CARLA autonomous agent.
	
	Attributes
	----------
	_lidar_layers : nn.Sequential
		Sequential of lidar neural network layers.
	_loc_layers : nn.Sequential
		Sequential of location neural network layers.
	_join_layers : nn.Sequential
		Sequential of sensor fusion neural network layers.
	
	Methods
	-------
	forward(sensors_data)
		Forward branch of neural network.
	"""
	
	def __init__(self):
		"""Initialization of neural network model.
		"""
		
		# init parent
		super().__init__()
		
		# init lidar layers
		self._lidar_layers = NNFullyConnPart(
			features = [60000, 500],
			dropout_probability  = [0.0],
		)
		
		# create location pass
		self._loc_layers = NNFullyConnPart(
			features = [5, 5000],
			dropout_probability  = [ 0.0],
		)
		
		# init join layers
		self._join_layers = NNFullyConnPart(
			features = [5500, 3],
			dropout_probability = [0.0],
			last_part = True
		)
		
	def forward(self, sensors_data):
		"""Forward branch for neural network.
		
		Parameters
		----------
		sensors_data : dict
			Dict with sensors data.
		
		Returns
		-------
		output : torch.Tensor
			Tensor of three floating points - steer, throttle, brake.
		"""
		
		# PREPARE DATA
		to_end = sensors_data['end_position'] - sensors_data['gps']
		from_start = sensors_data['gps'] - sensors_data['start_position']
		
		# create input data
		input_data = torch.cat((
			to_end[0:-1]*10000,
			from_start[0:-1]*10000,
			sensors_data['direction']
		))
		
		# create lidar input data
		lidar = sensors_data["lidar"]
		size = lidar.shape[0]
		
		if size > 15000:
			lidar = lidar[0:15000]
		else:
			diff = 15000 - size
			data_for_app = lidar[0:diff]
			lidar = torch.cat((lidar, data_for_app))
			
		lidar = lidar.transpose(1, 0)
		lidar_data = torch.flatten(lidar)
		
		# FORWARD PASS
		# lidar pass
		lidar_out = self._lidar_layers(lidar_data)
		
		# location pass
		location_out = self._loc_layers(input_data)
		
		# join data
		join_input = torch.cat((lidar_out, location_out))
		
		# join output pass
		control = self._join_layers(join_input)
		
		return control

