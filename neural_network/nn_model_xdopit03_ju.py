#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of neural_network.
#
# It contains class for neural network model. 
#
# Author:	Marek Dopita (xdopit03)
#

import torch.nn as nn
import torch

from .nn_fully_conn_part import NNFullyConnPart
from .nn_convolutional_part import NNConvolutinalPart

class NNModelXdopit03JU(nn.Module):
	"""Neural network model for CARLA autonomous agent junctions.
	
	Attributes
	----------
	_image_layers : nn.Sequential
		Sequential of camera neural network layers.
	_loc_layers : nn.Sequential
		Sequential of location neural network layers.
	_join_layers : nn.Sequential
		Sequential of sensor fusion neural network layers.
	
	Methods
	-------
	forward(sensors_data)
		Forward branch of neural network
	"""
	
	def __init__(self):
		"""Initialization of neural network model.
		"""
		
		# init parent
		super().__init__()
		
		# set image sizes
		IMAGE_SIZES = [3, 300, 400]
		
		# init convolutional part
		conv_part = NNConvolutinalPart(
			channels = [IMAGE_SIZES[0], 32, 64, 128, 256, 128, 64, 32, 5],
			kernel = [5, 5, 5, 3, 3, 3, 3, 3],
			stride = [2, 1, 2, 1, 2, 1, 1, 1],
			dropout_probability = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
		)
		
		# input features for fully connected part
		input_features = conv_part.simulate_output_size(IMAGE_SIZES)
		
		# init fully connected part of neural network
		fully_conn_part = NNFullyConnPart(
			features = [input_features, 5000],
			dropout_probability  = [ 0.0],
		)
		
		# create sequential model from layer list
		self._image_layers = nn.Sequential(*[conv_part, fully_conn_part])
		
		# create location pass
		self._loc_layers = NNFullyConnPart(
			features = [5, 5000],
			dropout_probability  = [ 0.0],
		)
		
		# init join layers
		self._join_layers = NNFullyConnPart(
			features = [10000, 3],
			dropout_probability = [0.0],
			last_part = True
		)
		
	def forward(self, sensors_data):
		"""Forward branch for neural network.
		
		Parameters
		----------
		sensors_data : dict
			Dict with data from sensors. 
		
		Returns
		-------
		array : torch.Tensor
			Tensor of three floating points - steer, throttle, brake.
		"""
		
		# PREPARE DATA
		to_end = sensors_data['end_position'] - sensors_data['gps']
		from_start = sensors_data['gps'] - sensors_data['start_position']
		
		# create input data
		input_data = torch.cat((
			to_end[0:-1]*10000,
			from_start[0:-1]*10000,
			sensors_data['direction']
		))
		
		c_image = sensors_data['c_image'].unsqueeze(0)
		
		# FORWARD PASS
		
		# image pass
		image_out = self._image_layers(c_image)
		image_out = torch.squeeze(image_out)
		
		# location pass
		location_out = self._loc_layers(input_data)
		
		# join data
		join_input = torch.cat((image_out, location_out))
		
		# fusion pass
		control = self._join_layers(join_input)
		
		return control
