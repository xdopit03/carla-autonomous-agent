#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of neural_network.
#
# It contains class for neural network model. 
#
# Author:	Marek Dopita (xdopit03)
#

import numpy as np
import cv2
import os

import torch.nn as nn
import torch

from .nn_convolutional_part import NNConvolutinalPart
from .nn_fully_conn_part import NNFullyConnPart

class NNModelXdopit03LF(nn.Module):
	"""Model of neural network for CARLA autonomous agent.
	
	Attributes
	----------
	_layers : nn.Sequential
		Sequential of neural network layers.
	
	Methods
	-------
	forward(sensors_data)
		Forward branch of neural network.
	"""
	
	def __init__(self):
		"""Initialization of neural network model.
		"""
		
		# init parent
		super().__init__()
		
		# set image sizes
		IMAGE_SIZES = [3, 300, 400]
		
		# init convolutional part
		conv_part = NNConvolutinalPart(
			channels = [IMAGE_SIZES[0], 32, 64, 128, 64, 32, 5],
			kernel = [5, 3, 3, 3, 3, 3],
			stride = [2, 1, 2, 1, 1, 1],
			dropout_probability = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
		)
		
		# input features for fully connected part
		input_features = conv_part.simulate_output_size(IMAGE_SIZES)
		
		# init fully connected part
		fully_conn_part = NNFullyConnPart(
			features = [input_features, 3],
			dropout_probability = [0.0],
			last_part = True
		)
		
		# create sequential of conv and fully connected parts
		self._layers = nn.Sequential(*[conv_part, fully_conn_part])
		
	def forward(self, sensors_data):
		"""Forward branch for neural network.
		
		Parameters
		----------
		sensors_data : dict
			Dict with sensors data.
		
		Returns
		-------
		output : torch.Tensor
			Tensor of three floating points - steer, throttle, brake.
		"""
		
		# PREPARE DATA
		image_data = sensors_data['c_image'].unsqueeze(0)
		
		# pass data to nn
		output = self._layers(image_data)
		
		# squeeze output
		output = torch.squeeze(output)
		
		return output
