#!/bin/bash

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of scripts.
#
# It contains script for dataset parsing. 
#
# Author:	Marek Dopita (xdopit03)
#

# read paths
read dataset
read new_dataset

dataset_path="$DATASETS_ROOT/$dataset"
new_dataset_path="$DATASETS_ROOT/$new_dataset"

# check if path to collected dataset exists
if [[ ! -d $dataset_path || $dataset_path == "" ]]
then
	echo "Collected dataset path does not exists." >&2
	exit 1
fi

# check if path to target dataset exists
if [[ ! -d $new_dataset_path || $new_dataset_path == "" ]]
then
	echo "Targeted dataset path does not exists." >&2
	exit 1
fi

# if situations does not exists
if [ ! -d $new_dataset_path/change_lane ]
then
	mkdir $new_dataset_path/change_lane
	mkdir $new_dataset_path/junction
	mkdir $new_dataset_path/lane_follow
	mkdir $new_dataset_path/preloads
fi

# get paths of episodes
episode_count=$(find $dataset_path -maxdepth 1 -name 'episode_*' | wc -l)

# check if contains episodes
if [ $episode_count -eq 0 ]
then
	echo "No episodes in dataset."
	exit 1
fi

# read program and call
while read line
do
	echo $line
	${TEAM_CODE_ROOT}/scripts/reshape_dataset_part.sh $dataset_path $new_dataset_path $line
done
