#!/bin/bash

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of scripts.
#
# It contains script for run validation. 
#
# Author:	Marek Dopita (xdopit03)
#

# check if parameters exists
if [[ $# -ne 2 ]]
then
	echo "Expected 2 parameters:" >&2
	echo "#1 - model type { xdopit03_cl | xdopit03_ju | xdopit03_lf }" >&2
	echo "#2 - dataset folder in datasets directory" >&2
	exit 1
fi

# check if parameter is low or epic
if [[ $1 != "xdopit03_cl" && $1 != "xdopit03_ju" && $1 != "xdopit03_lf" ]]
then
	echo "Expected first parameter { xdopit03_cl | xdopit03_ju | xdopit03_lf }." >&2
	exit 1
fi

# create dataset path
dataset_path="$DATASETS_ROOT/$2"

# check if path to collected dataset exists
if [[ ! -d $dataset_path || $dataset_path == "" ]]
then
	echo "Path to dataset does not exists." >&2
	exit 1
fi

# xdopit03 change line validation
if [[ $1 == "xdopit03_cl" ]]
then
	$TEAM_CODE_ROOT/training/run_validation.py --model=model_xdopit03_cl --load_model=xdopit03_trained_model_cl.pt \
	--dataset=xdopit03_d_cl --dataset_path=$dataset_path
	
# xdopit03 junction validation
elif [[ $1 == "xdopit03_ju" ]]
then
	$TEAM_CODE_ROOT/training/run_validation.py --model=model_xdopit03_ju --load_model=xdopit03_trained_model_ju.pt \
	--dataset=xdopit03_d_ju --dataset_path=$dataset_path
		
	# xdopit03 lane follow validation
else
	$TEAM_CODE_ROOT/training/run_validation.py --model=model_xdopit03_lf --load_model=xdopit03_trained_model_lf.pt \
	--dataset=xdopit03_d_lf --dataset_path=$dataset_path
fi

