#!/bin/bash

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of scripts.
#
# It contains script for autonomous agent run. 
#
# Author:	Marek Dopita (xdopit03)
#

# check if parameters exists
if [[ $# -ne 4 ]]
then
	echo "Expected 4 parameters" >&2
	
	s_files="$(ls ${TEAM_CODE_ROOT}drive_scenarios/ | grep .*.json | tr "\n" " ")"
	r_files="$(ls ${TEAM_CODE_ROOT}drive_routes/ | grep .*.xml | tr "\n" " ")"
	c_files="$(ls ${TEAM_CODE_ROOT}autonomous_agents/ | grep .*.txt | tr "\n" " ")"
	a_files="$(ls ${TEAM_CODE_ROOT}autonomous_agents/ | grep .*.py | grep -v __pycache__ | grep -v __init__.py | tr "\n" " ")"
	
	echo "#1 - scenario files:	$s_files" >&2
	echo "#2 - route files:	$r_files" >&2
	echo "#3 - config files:	$c_files" >&2
	echo "#4 - agent files:	human $a_files" >&2
	exit 1
fi

# create file names
SCENARIO_FILE=${TEAM_CODE_ROOT}drive_scenarios/$1
ROUTE_FILE=${TEAM_CODE_ROOT}drive_routes/$2
CONFIG_FILE=${TEAM_CODE_ROOT}autonomous_agents/$3
AUTONOMOUS_AGENT=$4
AGENT_FILE=${TEAM_CODE_ROOT}autonomous_agents/$4

# check scenario file
if [ ! -f "$SCENARIO_FILE" ]
then
	echo "$SCENARIO_FILE does not exist." >&2
	exit 1
fi

# check route file
if [ ! -f "$ROUTE_FILE" ]
then
	echo "$ROUTE_FILE does not exist." >&2
	exit 1
fi

# check config file
if [ ! -f "$CONFIG_FILE" ]
then
	echo "$CONFIG_FILE does not exist." >&2
	exit 1
fi

if [ ! $AUTONOMOUS_AGENT == "human" ]
then
	if [ ! -f "$AGENT_FILE" ]
	then
		echo "$AGENT_FILE does not exist." >&2
		exit 1
	fi
fi

# export variables
export SCENARIOS=$SCENARIO_FILE
export ROUTES=$ROUTE_FILE
export REPETITIONS=1
export DEBUG_CHALLENGE=0
export CHALLENGE_TRACK_CODENAME=SENSORS

# set agent
if [[ $AUTONOMOUS_AGENT == "human" ]]
then
	export TEAM_AGENT=${LEADERBOARD_ROOT}/leaderboard/autoagents/human_agent.py
	export CHECKPOINT_ENDPOINT=${TEAM_CODE_ROOT}/drive_results/human_agent.json
else
	export TEAM_AGENT=$AGENT_FILE
	export CHECKPOINT_ENDPOINT=${TEAM_CODE_ROOT}/drive_results/autonomous_agent_${AUTONOMOUS_AGENT%%.*}.json
	export TEAM_CONFIG=$CONFIG_FILE
fi

# run leaderboard evaluation
${LEADERBOARD_ROOT}/scripts/run_evaluation.sh
