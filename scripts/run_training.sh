#!/bin/bash

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of scripts.
#
# It contains script for run training. 
#
# Author:	Marek Dopita (xdopit03)
#

# check if parameters exists
if [[ $# -ne 3 ]]
then
	echo "Expected 3 parameters:" >&2
	echo "#1 - model type { xdopit03_cl | xdopit03_ju | xdopit03_lf }" >&2
	echo "#2 - number of training iterations" >&2
	echo "#3 - dataset folder in datasets directory" >&2
	exit 1
fi

# check if parameter is low or epic
if [[ $1 != "xdopit03_cl" && $1 != "xdopit03_ju" && $1 != "xdopit03_lf" ]]
then
	echo "Expected first parameter { xdopit03_cl | xdopit03_ju | xdopit03_lf }." >&2
	exit 1
fi

# check second parametr for number
re='^[0-9]+$'
if ! [[ $2 =~ $re ]]
then
	echo "Expected second parameter to be a number." >&2
	exit 1
fi

# create dataset path
dataset_path="$DATASETS_ROOT/$3"

# check if path to collected dataset exists
if [[ ! -d $dataset_path || $dataset_path == "" ]]
then
	echo "Path to dataset does not exists." >&2
	exit 1
fi

for i in $(eval echo "{1..$2}")
do
	now="$(date +'%d.%m.%Y %H:%M:%S')"
	echo ""
	echo "$now - Epoch: $i"
	
	# xdopit03 change line training
	if [[ $1 == "xdopit03_cl" ]]
	then
		$TEAM_CODE_ROOT/training/run_training.py --model=model_xdopit03_cl --save_model=xdopit03_trained_model_cl \
		--dataset=xdopit03_d_cl --dataset_path=$dataset_path --load_model=xdopit03_trained_model_cl.pt
		
	# xdopit03 junction training
	elif [[ $1 == "xdopit03_ju" ]]
	then
		$TEAM_CODE_ROOT/training/run_training.py --model=model_xdopit03_ju --save_model=xdopit03_trained_model_ju \
		--dataset=xdopit03_d_ju --dataset_path=$dataset_path --load_model=xdopit03_trained_model_ju.pt
		
	# xdopit03 lane follow training
	else
		$TEAM_CODE_ROOT/training/run_training.py --model=model_xdopit03_lf --save_model=xdopit03_trained_model_lf \
		--dataset=xdopit03_d_lf --dataset_path=$dataset_path --load_model=xdopit03_trained_model_lf.pt
	fi
done

