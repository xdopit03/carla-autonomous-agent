#!/bin/bash

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of scripts.
#
# It contains script for CARLA simulator run. 
#
# Author:	Marek Dopita (xdopit03)
#

# check if parameter exists
if [[ $# -ne 1 ]]
then
	echo "Expected one parameter { Epic | Low }." >&2
	exit 1
fi

# check if parameter is low or epic
if [[ $1 != "Low" && $1 != "Epic" ]]
then
	echo "Expected one parameter { Epic | Low }." >&2
	exit 1
fi

# run carla simulator
${CARLA_ROOT}/CarlaUE4.sh -quality-level=$1 -world-port=2000 -resx=800 -resy=600
exit 0
