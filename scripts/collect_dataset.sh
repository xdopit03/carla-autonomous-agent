#!/bin/bash

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of scripts.
#
# It contains script for autonomous agent run. 
#
# Author:	Marek Dopita (xdopit03)
#

# check if parameters exists
if [[ $# -ne 1 ]]
then
	echo "Expected 1 parameter" >&2
	echo "#1 - folder in dataset path" >&2
	exit 1
fi

# create dataset path
dataset_path="$DATASETS_ROOT/$1"

# check if path to collected dataset exists
if [[ ! -d $dataset_path || $dataset_path == "" ]]
then
	echo "Path to save collected dataset does not exist." >&2
	exit 1
fi

# setup
export SCENARIOS=${LEADERBOARD_ROOT}/data/all_towns_traffic_scenarios_public.json
export ROUTES=${LEADERBOARD_ROOT}/data/routes_training.xml
export REPETITIONS=1
export DEBUG_CHALLENGE=0
export TEAM_AGENT=${TEAM_CODE_ROOT}/datasets/dataset_collector_leaderboard_agent.py
export CHECKPOINT_ENDPOINT=${TEAM_CODE_ROOT}/drive_results/dataset_collector_results.json
export CHALLENGE_TRACK_CODENAME=SENSORS
export TEAM_CONFIG=$dataset_path

# run leaderboard evaluation
${LEADERBOARD_ROOT}/scripts/run_evaluation.sh
