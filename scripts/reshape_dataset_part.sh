#!/bin/bash

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of scripts.
#
# It contains script for dataset part parsing. 
#
# Author:	Marek Dopita (xdopit03)
#

# check if parameters exists
if [[ $# -ne 7 ]]
then
	echo "Expected 7 parameters:" >&2
	echo "#1 - path to collected dataset" >&2
	echo "#2 - path to new dataset where data will be moved" >&2
	echo "#3 - name of episode" >&2
	echo "#4 - target situation { change_lane | junction | lane_follow }" >&2
	echo "#5 - name of part to generate" >&2
	echo "#6 - measurement start id" >&2
	echo "#7 - measure end id" >&2
	exit 1
fi

# set variables
dataset_path=$1
dataset_target=$2
episode_number=$3
target_situation=$4
part_name=$5
measure_start=$6
measure_end=$7

# check if path to collected dataset exists
if [ ! -d $dataset_path ]
then
	echo "Collected dataset path does not exists." >&2
	exit 1
fi

# check if path to target dataset exists
if [ ! -d $dataset_target ]
then
	echo "Targeted dataset path does not exists." >&2
	exit 1
fi

#create episode path
episode_path="$dataset_path/$episode_number"

# check if episode exists
if [ ! -d $episode_path ]
then
	echo "Episode does not exists in collected dataset." >&2
	exit 1
fi

#regex for check part name
reg=""

# check target_situation and set regex
if [ $target_situation == "change_lane" ]
then
	reg="^part_[0-9]{5}_(left|right)$"
elif [ $target_situation == "junction" ]
then
	reg="^part_[0-9]{5}_(left|right|straight)$"
elif [ $target_situation == "lane_follow" ]
then
	reg="^part_[0-9]{5}$"
else
	echo "Expected situation { change_lane | junction | lane_follow }." >&2
	exit 1
fi

# check part name
if ! [[ $part_name =~ $reg ]]
then
	echo "Expected part name with format: '$reg' for $target_situation." >&2
	exit 1
fi

# regex for measurement id
re="^[0-9]{5}$"

# check start measurement number
if ! [[ $measure_start =~ $re ]]
then
	echo "Expected measurement start id with format: '$re'." >&2
	exit 1
fi

# check end measurement number
if ! [[ $measure_end =~ $re ]]
then
	echo "Expected measurement end id with format: '$re'." >&2
	exit 1
fi

# create episode in new dataset when does not exists
mkdir -p "$dataset_target/change_lane/$episode_number/"
mkdir -p "$dataset_target/junction/$episode_number/"
mkdir -p "$dataset_target/lane_follow/$episode_number/"

# create part
part_directory="$dataset_target/$target_situation/$episode_number/$part_name"

# check if part exists
if [ -d $part_directory ]
then
	echo "Part already exists in new dataset." >&2
	exit 1
fi

# create part directory
mkdir $part_directory

# move data
for i in $(eval echo "{$measure_start..$measure_end}")
do
	mv $dataset_path/$episode_number/*_$i.* $part_directory
done
