#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of autonomous_agents.
#
# It contains autonomous agent class. 
#
# Author:	Marek Dopita (xdopit03)
#

import os
import torch
import carla
import cv2
import numpy as np
import carla

from leaderboard.autoagents.autonomous_agent import AutonomousAgent, Track
from agents.navigation.local_planner import RoadOption
from neural_network import Model
from utilities import AgentException

def get_entry_point():
	"""Getter for agent class name.
	
	Returns
	-------
	str
		Agent class name
	"""
	
	return 'AgentBase'

class AgentBase(AutonomousAgent):
	"""Base autonomous agent class.
	
	Attributes
	----------
	_model_cl
		Neural network model for lange change.
	_model_ju
		Neural network model for junctions.
	_model_lf
		Neural network model for lane follow.
	_current_point
		Actual route point.
	_current_point_index
		Index of actual route point in route list.
	_next_point
		Next route point.
	_point_deviation
		Deviation from route point.
	_current_state
		Current drive state.
	_route_points_count
		Number of route points.
	
	Methods
	-------
	setup()
		Prepare agent for driving.
	sensors()
		Setup sensors on vehicle.
	run_step()
		One step of drive simulation.
	_parse_sensor_data()
		Parsing data from sensor for neural network.
	_parse_model_output()
		Parsing output of neural network.
	_init_agent()
		Init autonomous agent at first step.
	_get_route_point()
		Getting route point from route list.
	_hit_route_point()
		Check if car is in targeted route point.
	_set_next_point()
		Set new target point.
	"""
	
	def setup(self, path_to_conf_file):
		"""Initializations and definitions for agent.
		
		Parameters
		----------
		path_to_conf_file : str
			Path to configuration file with neural network model.
		"""
		
		# set agent track SENSORS/MAP
		self.track = Track.SENSORS
		
		# check configuration file
		if not os.path.exists(path_to_conf_file):
			raise AgentException('invalid_path')
		
		# read configuration
		with open(path_to_conf_file, 'r') as config_file:
			cl_file_path = os.path.expandvars(config_file.readline().strip())
			ju_file_path = os.path.expandvars(config_file.readline().strip())
			lf_file_path = os.path.expandvars(config_file.readline().strip())
		
		# if paths does not exists
		if not os.path.exists(cl_file_path) or not os.path.exists(ju_file_path) or not os.path.exists(lf_file_path):
			raise AgentException('invalid_model_path')
		
		# load saved files with models
		state_cl = torch.load(cl_file_path)
		state_ju = torch.load(ju_file_path)
		state_lf = torch.load(lf_file_path)
		
		# create models
		self._model_cl = Model("model_xdopit03_cl")
		self._model_ju = Model("model_xdopit03_ju")
		self._model_lf = Model("model_xdopit03_lf")
		
		# load models from save
		self._model_cl.load_state_dict(state_cl['model'])
		self._model_ju.load_state_dict(state_ju['model'])
		self._model_lf.load_state_dict(state_lf['model'])
		
		# set evaluation mode
		self._model_cl.eval()
		self._model_ju.eval()
		self._model_lf.eval()
		
		# last passed route point
		self._current_point = None
		self._current_point_index = None
		
		# targeted point
		self._next_point = None
		
		# max deviation from next point
		self._point_deviation = 0.00001
		
		# current driving state
		self._current_state = None
		
		# count of route points
		self._route_points_count = 0
		
	def sensors(self):
		"""Definitions for agent senzors.
		
		Returns
		-------
		sensors : dict
			Dict of sensor definitions.
		"""
		
		sensors = [
			{'type': 'sensor.camera.rgb','id': 'CameraCentral', 'x': 2.0, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': -15.0, 'yaw': 0.0, 'width': 800, 'height': 600, 'fov': 100},
			{'type': 'sensor.camera.rgb','id': 'CameraLeft', 'x': 2.0, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': -15.0, 'yaw': -30.0, 'width': 800, 'height': 600, 'fov': 100},
			{'type': 'sensor.camera.rgb','id': 'CameraRight', 'x': 2.0, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': -15.0, 'yaw': 30.0, 'width': 800, 'height': 600, 'fov': 100},
			{'type': 'sensor.lidar.ray_cast','id': 'Lidar', 'x': 0.0, 'y': 0.0, 'z': 2.5, 'roll': 0.0, 'pitch': 0.0, 'yaw': 0.0},
			{'type': 'sensor.other.gnss', 'id': 'GPS', 'x': 0.0, 'y': 0.0, 'z': 2.5},
			{'type': 'sensor.other.imu', 'id': 'IMU', 'x': 0.7, 'y': -0.4, 'z': 1.60, 'roll': 0.0, 'pitch': 0.0, 'yaw': -45.0},
			{'type': 'sensor.speedometer', 'id': 'Speed'},
			{'type': 'sensor.other.radar', 'id': 'RADARFront', 'x': 2.0, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': 0.0, 'yaw': 0.0, 'fov': 100},
			{'type': 'sensor.other.radar', 'id': 'RADARBack', 'x': -2.2, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': 0.0, 'yaw': 180.0, 'fov': 100},
		]
		
		return sensors
		
	def run_step(self, input_data, timestamp):
		"""Produce new action for carla.VehicleControl object.
		
		Parameters
		----------
		input_data : dict
			Data from sensors.
		timestamp : float
			Timestamp value of simulation.
			
		Raises
		------
		AgentException
			Agent exception when simulator is in invalid state.
			
		Returns
		-------
		control : carla.VehicleControl
			Control of vehicle.
		"""
		
		# init when first step
		if self._current_state == None:
			self._init_agent()
		
		# parse senzor data
		parsed_data, gps_location = self._parse_sensor_data(input_data)
		
		# if car in route point change state and target
		if self._hit_route_point(gps_location):
			self._set_next_point()
		
		# switch drive state
		if self._current_state == RoadOption.LANEFOLLOW:
			print('LANEFOLLOW')
			
			# forward branch of model
			model_output = self._model_lf(parsed_data)
			
		elif self._current_state == RoadOption.LEFT:
			print('LEFT')
			
			# add direction
			parsed_data['direction'] = torch.Tensor(np.array([-1]))
			
			# forward branch of model
			model_output = self._model_ju(parsed_data)
			
		elif self._current_state == RoadOption.RIGHT:
			print('RIGHT')
			
			# add direction
			parsed_data['direction'] = torch.Tensor(np.array([1]))
			
			# forward branch of model
			model_output = self._model_ju(parsed_data)
			
		elif self._current_state == RoadOption.STRAIGHT:
			print('STRAIGHT')
			
			# add direction
			parsed_data['direction'] = torch.Tensor(np.array([0]))
			
			# forward branch of model
			model_output = self._model_ju(parsed_data)
			
		elif self._current_state == RoadOption.CHANGELANELEFT:
			print('CHANGELANELEFT')
			
			# add direction
			parsed_data['direction'] = torch.Tensor(np.array([-1]))
			
			# forward branch of model
			model_output = self._model_cl(parsed_data)
			
		elif self._current_state == RoadOption.CHANGELANERIGHT:
			print('CHANGELANERIGHT')
			
			# add direction
			parsed_data['direction'] = torch.Tensor(np.array([1]))
			
			# forward branch of model
			model_output = self._model_cl(parsed_data)
			
		else:
			raise AutonomousAgent('wrong_state')
		
		# get vehicle control from model output
		steer, throttle, brake = self._parse_model_output(model_output)
		
		# set vehicle control
		control = carla.VehicleControl()
		control.steer = steer
		control.brake = brake
		control.throttle = throttle
		control.hand_brake = False
		
		return control
		
	def _parse_sensor_data(self, sensors_data):
		"""Preproccess sensor data from carla simulator.
		
		Parameters
		----------
		sensors_data : dict
			Dict with sensor data from carla.
			
		Returns
		-------
		front_camera : torch.Tensor
			Tensor from front camera.
		gps_location : dict
			Dict of gps location.
		"""
		
		# front camera parsing
		c_image = sensors_data['CameraCentral'][1]
		
		c_image = cv2.resize(c_image, (400,300))
		c_image = c_image.transpose(2, 0, 1)
		c_image = c_image.astype(np.float)
		c_image = torch.Tensor(c_image[0:-1])
		c_image /= 255.0
		
		# GPS parsing
		gps = {'lat': sensors_data['GPS'][1][0], 'lon': sensors_data['GPS'][1][1]} 
		gps_torch = torch.squeeze(torch.Tensor(np.array([sensors_data['GPS'][1]])), 0)
		
		# get end and start position
		end_position = torch.Tensor(np.array([self._next_point['lat'], self._next_point['lon'], 0.]))
		if self._current_point:
			start_position = torch.Tensor(np.array([self._current_point['lat'], self._current_point['lon'], 0.]))
		else:
			start_position = None
		
		# LiDAR parsing
		lidar_torch = torch.squeeze(torch.Tensor(np.array([sensors_data['Lidar'][1]])), 0)
		
		# parsed data dict
		parsed_data = {
			'gps': gps_torch,
			'c_image': c_image,
			'end_position': end_position,
			'start_position': start_position,
			'lidar': lidar_torch
		}
		
		return parsed_data, gps 
		
	def _parse_model_output(self, output):
		"""Reformat model output into steer, throttle and brake.
		
		Parameters
		----------
		output : torch.Tensor
			Output of neural network model.
			
		Returns
		-------
		steer : float
			Value of steer between -1 and 1.
		throttle : float
			Value of throttle between 0 and 1.
		brake : float
			Value of brake between 0 and 1.
		"""
		
		# get data from tensor
		steer = output[2].item()
		throttle = output[0].item()
		brake = output[1].item()
		
		# if throttle and try brake at same time
		if brake < throttle:
			brake = 0.0
		else:
			throttle = 0.0
		
		if self._current_state == RoadOption.LEFT or self._current_state == RoadOption.RIGHT:
			steer *= 1.1
		
		return steer, throttle, brake
		
	def _init_agent(self):
		"""Init first run of autonomous agent, prepare route points, set current agent state.
		"""
		
		# number of route points 
		self._route_points_count = len(self._global_plan)
		
		# route must have at least start and end point
		if self._route_points_count < 2:
			raise AgentException('invalid_route_plan')
		
		# set start point as current point
		self._current_point, self._current_state = self._get_route_point(0)
		
		# set next point as target
		self._next_point, _ = self._get_route_point(1)
		
		# current point index is 0
		self._current_point_index = 0
		
	def _get_route_point(self, index):
		"""Get route point and next agent state from global plan at defined index, throw exception when index is out of bounds.
		
		Parameters
		----------
		index : int
			Index of route point in route plan.
		
		Raises
		------
		AgentException
			Raise agent exception when route index is out of bounds.
		
		Returns
		-------
		position : dict
			Position of route point on defined index.
		"""
		
		# if index is out of bounds
		if index < 0 or index > self._route_points_count - 1:
			raise AgentException('route_index_out_of_bounds')
		
		# return dictionary with longtitute and latitude of route point and agent state from that point
		return {'lat': self._global_plan[index][0]['lat'], 'lon': self._global_plan[index][0]['lon']}, self._global_plan[index][1]
		
	def _hit_route_point(self, gps_location):
		"""Check if autonomous agent is in next targeted route point with given deviation, location of agent is gps_location variable.
		
		Parameters
		----------
		gps_location : dict
			Dictionary with car gps location.
		
		Returns
		-------
		route_hit : bool
			True when gps_location near next route point.
		"""
		
		# return true when car is near defined point or false
		return abs(self._next_point['lat'] - gps_location['lat']) < self._point_deviation and \
				abs(self._next_point['lon'] - gps_location['lon']) < self._point_deviation
		
	def _set_next_point(self):
		"""Autonomous agent set targeted point as current, and set new target point.
		"""
		
		# set new current point
		self._current_point, self._current_state = self._get_route_point(self._current_point_index)
		
		# increment point index
		self._current_point_index += 1
		
		# set new targeted point
		self._next_point, _ = self._get_route_point(self._current_point_index) 
