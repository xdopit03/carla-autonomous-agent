#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of utilities.
#
# It contains basic functions used in different places of carla leaderboad team code. 
#
# Author:	Marek Dopita (xdopit03)
#

import re
import sys
import datetime

def try_parse_int(string):
	"""Trying parse string to int, if fails, returns string.
	
	Parameters
	----------
	string : str
		String for conversion try to int.
	
	Returns
	-------
	int or str
		Returns int of string if conversion success, else returns basic string. 
	"""
	
	try:
		return int(string)
	except:
		return string
	
def alphanumeric_key(string):
	"""Transform string into list of strings and integers .
	
	Parameters
	----------
	string : str
		String for split.
	
	Returns
	-------
	list
		List of strings and integers from string.
	"""
	
	return [try_parse_int(part) for part in re.split('([0-9+])', string)]
	
def print_error(message):
	"""Print message to standard error.
	
	Parameters
	----------
	message : str
		Message to be printed.
	"""
	
	print(message + '\nFor more info run with option -h / --help.', file = sys.stderr)
	
def log(message):
	"""Print log with actual date.
	
	Parameters
	----------
	message : str
		Text message to log.
	"""
	
	date = datetime.datetime.now()
	
	print(date.strftime("%d.%m.%Y %H:%M:%S"), "-", message)
	
def progres_print(count, total, bar_len):
	"""Print progress par to stdout.
	
	Parameters
	----------
	count : int
		Count of completed units.
	total : int
		Total count of units.
	bal_len : int
		Width of terminal window.
	"""
	
	# lenght of filled status
	filled_len = int(round(bar_len * count / float(total)))
	
	# create filled bar
	percents = round(100.0 * count / float(total), 1)
	bar = '=' * filled_len + '-' * (bar_len - filled_len)
	
	# format
	fmt = '[%s] %s%%' % (bar, percents)
	print('\b' * len(fmt), end='')
	
	# print to stdout
	sys.stdout.write(fmt)
	sys.stdout.flush()
