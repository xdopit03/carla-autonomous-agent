#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of utilities.
#
# It contains special exception classes for team code. 
#
# Author:	Marek Dopita (xdopit03)
#

class AgentException(Exception):
	"""Exception class for autonomous agent.
	
	Attributes
	----------
	_type_name : str
		Name of error message type.
	"""
	
	def __init__(self, type_name):
		"""Initialization of exception class, set type of agent exception.
		
		Parameters
		----------
		type_name : str
			Type name of erro message.
		"""
		self._type_name = type_name
	
	def __str__(self):
		"""Return description of raised exception.
		"""
		
		if self._type_name == 'invalid_route_plan':
			return 'Route plan for agent is invalid!'
			
		elif self._type_name == 'route_index_out_of_bounds':
			return 'Agent tried to acces wrong route point, out of bounds!'
			
		elif self._type_name ==  'wrong_state':
			return 'Agent in invalid state!'
			
		elif self._type_name == 'invalid_path':
			return 'Configuration file does not exists!'
			
		elif self._type_name == 'invalid_model_path':
			return 'Trained neural network files defined in configuration file does not exists!'
		
		else:
			return 'Autonomous agent error!'
			
class ArgumentParsingException(Exception):
	""" Exception class for argument parsing.
	
	Attributes
	----------
	_description : str
		Text description of exception.
	"""
	
	def __init__(self, description):
		"""Initialization of exception class, set exception description.
		
		Parameters
		----------
		description : str
			Text description of exception.
		"""
		
		self._description = description
	
	def __str__(self):
		"""Return description of exception.
		"""
		
		return self._description
		
class UndefinedException(Exception):
	""" Exception class accesing undefined team code parts.
	
	Attributes
	----------
	_description : str
		Text description of exception.
	"""
	
	def __init__(self, description):
		"""Initialization of exception class, set exception description.
		
		Parameters
		----------
		description : str
			Text description of exception.
		"""
		
		self._description = description
		
	def __str__(self):
		"""Return description of exception.
		"""
		
		return self._description
		
class NetException(Exception):
	"""Exception class for neural network.
	
	Attributes
	----------
	_description : str
		Text description of exception.
	"""
	
	def __init__(self, description):
		"""Initialization of exception class, set exception description.
		
		Parameters
		----------
		description : str
			Text description of exception.
		"""
		
		self._description = description
		
	def __str__(self):
		"""Return description of exception.
		"""
		
		return self._description
