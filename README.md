# CARLA autonomní agent pro CARLA Leaderboard

- **Autor:** Marek Dopita (xdopit03)

## CARLA Leaderboard

Implementuje projekt CARLA žebříček. Více informací na [tomto odkaze](https://leaderboard.carla.org/).

## Obsah

| Složka | Obsah složky |
|--------|-----------------------------------------------|
| autonomous\_agents  | Program s autonomním agentem, který za použití neuronových sítí a senzorů ovládá vozidlo. |
| datasets | Programy pro získávání a třídění datových sad. |
| drive\_results  | Výsledky vyhodnocení agenta. |
| drive\_routes  | Definice dostupných cest k řízení. |
| drive\_scenarios  | Definice scénářů, které se objeví na definovaných trasách. |
| neural\_network  | Definice modelů neuronových sítí. |
| scripts  | Složka se skripty pro spouštění částí projektu. |
| trained\_models  | Obsahuje natrénované modely neuronových sítí, které budou použity k řízení. |
| training  | Programy pro trénink a validaci neuronových sítí. |
| utilities | Pomocné funkce a třídy využívané v projektu. |
| README.md | Popis projektu. |
| requirements.txt | Popis závislostí projektu. |

## Instalace závislostí

Pro instalaci závislostí je připraven soubor. 

Spouštění instalace:

pip3 install -r requirements.txt

