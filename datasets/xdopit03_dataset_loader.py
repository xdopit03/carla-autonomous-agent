#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of datasets.
#
# It contains class for dataset created for project. 
#
# Author:	Marek Dopita (xdopit03)
#

import glob
import os
import json
import numpy as np
import torch
import cv2

from utilities import alphanumeric_key, log

class Xdopit03DatasetLoader:
	"""Class for dataset loading from XDOPIT03 carla dataset.
	
	Attributes
	----------
	_subtype : str
		Type of dataset situation - change line, junction, lane follow.
	_directory : str
		Directory of dataset.
	_c_cam_paths : list
		List of center camera paths.
	_l_cam_paths : list
		List of left camera paths.
	_r_cam_paths : list
		List of right camera paths.
	_other_paths : list
		List of other sensors file paths.
	_start_end_positions : list
		List of start end end positions when junction or change lane situation.
	
	Methods
	-------
	_load_data()
		Preload data.
	_get_start_end_positions()
		Get startind and ending positions when in junction or changing lane.
	"""
	
	def __init__(self, dataset_directory, subtype):
		"""Initialization of dataset object.
		
		Parameters
		----------
		dataset_directory : str
			Path to dataset directory.
		subtype : str
			Type of dataset situation - change line, junction, lane follow.
		"""
		
		# dataset preloaded data
		self._c_cam_paths = []
		self._l_cam_paths = []
		self._r_cam_paths = []
		self._other_paths = []
		self._start_end_positions = []
		
		# check if dataset directory exists
		if not os.path.exists(dataset_directory):
			log("Dataset directory does not exists.")
			return
		
		# set directory and preloads
		self._directory = dataset_directory
		self._subtype = subtype
		
		# check if town exists
		if not os.path.exists(os.path.join(dataset_directory, "preloads")) or not os.path.exists(os.path.join(dataset_directory, subtype)):
			log("Folder does not contains data - skip")
			return
		
		# create preload path
		preload_path = os.path.join(dataset_directory, "preloads" , subtype + "_preload.npy")
		
		# load data
		if not os.path.exists(preload_path):
			c_cam_paths, l_cam_paths, r_cam_paths, other_paths, start_end_positions = self._load_data()
			self._c_cam_paths += c_cam_paths
			self._l_cam_paths += l_cam_paths
			self._r_cam_paths += r_cam_paths
			self._other_paths += other_paths
			self._start_end_positions += start_end_positions
			log("Dataset " + subtype + " loaded with " + str(len(other_paths)) + " measurements.")
		else:
			c_cam_paths, l_cam_paths, r_cam_paths, other_paths, start_end_positions = np.load(preload_path, allow_pickle=True)
			self._c_cam_paths += c_cam_paths.tolist()
			self._l_cam_paths += l_cam_paths.tolist()
			self._r_cam_paths += r_cam_paths.tolist()
			self._other_paths += other_paths.tolist()
			self._start_end_positions += start_end_positions.tolist()
			log("Dataset " + subtype + " loaded with " + str(len(other_paths)) + " measurements from preload.")
		
		# filter data - solving fast changes in dataset - takes every third step
		self._c_cam_paths = self._c_cam_paths[::3]
		self._l_cam_paths = self._l_cam_paths[::3]
		self._r_cam_paths = self._r_cam_paths[::3]
		self._other_paths = self._other_paths[::3]
		self._start_end_positions = self._start_end_positions[::3]
		
	def __len__(self):
		"""Return length of object.
		
		Returns
		-------
		lenght : int
			Length of xdopit03 dataset object.
		"""
		return len(self._c_cam_paths)
		
	def __getitem__(self, index):
		"""Get item from object at index.
		
		Parameters
		----------
		index : int
			Index into class object.
			
		Returns
		-------
		data : dict
			Dictionary with dataset data.
		"""
		
		# load other sensors data
		sensor_data = np.load(self._other_paths[index], allow_pickle=True).item(0)
		
		# if changing direction - get this direction
		if self._subtype == 'junction' or self._subtype == 'change_lane':
			
			# get file path
			any_path = self._other_paths[index]
			
			# get direction
			direction = any_path.split('/')[-2].split('_')[-1]
			
			# direction to numpy value
			if direction == 'straight':
				np_direction = np.array([0])
			elif direction == 'left':
				np_direction = np.array([-1])
			else:
				np_direction = np.array([1])
			
			# set direction to data
			sensor_data['direction'] = np_direction
			sensor_data['start_position'] = self._start_end_positions[index]['start_position']
			sensor_data['end_position'] = self._start_end_positions[index]['end_position']
		
		# torch data
		torch_data = {}
		
		# forach dict create from numpy arrays torch tensors
		for key, value in sensor_data.items():
			if key == 'speed':
				value = np.array([value['speed']])
			
			torch_data[key] = torch.Tensor(value)
		
		# load images
		c_image = cv2.imread(self._c_cam_paths[index])
		l_image = cv2.imread(self._l_cam_paths[index])
		r_image = cv2.imread(self._r_cam_paths[index])
		
		# resize image
		c_image = cv2.resize(c_image, (400,300))
		l_image = cv2.resize(l_image, (400,300))
		r_image = cv2.resize(r_image, (400,300))
		
		# transpose matrixes
		# from [ [r,g,b], [r,g,b] ] -> [ [r,r], [g,g], [b,b]]
		c_image = c_image.transpose(2, 0, 1)
		l_image = l_image.transpose(2, 0, 1)
		r_image = r_image.transpose(2, 0, 1)
		
		# convert data in matrix from int to float
		c_image = c_image.astype(np.float)
		l_image = l_image.astype(np.float)
		r_image = r_image.astype(np.float)
		
		# convert numpy matrix to torch matrix
		c_image = torch.Tensor(c_image)
		l_image = torch.Tensor(l_image)
		r_image = torch.Tensor(r_image)
		
		# all data convert to 0..1
		c_image /= 255.0
		l_image /= 255.0
		r_image /= 255.0
		
		# set images to data for return
		torch_data['c_image'] = c_image
		torch_data['l_image'] = l_image
		torch_data['r_image'] = r_image
		
		return torch_data
		
	def _load_data(self):
		"""Preload data from dataset. 
			
		Returns
		-------
		c_cam_paths : list
			List with center camera image paths.
		l_cam_paths : list
			List with left camera image paths.
		r_cam_paths : list
			List with right camera image paths.
		other_paths : list
			List with other sensors paths.
		start_end_positions : list
			List with start and end positions.
		"""
		
		# return values
		c_cam_paths = []
		l_cam_paths = []
		r_cam_paths = []
		other_paths = []
		start_end_positions = []
		
		# get ordered episode paths
		episode_paths = glob.glob(os.path.join(self._directory, self._subtype, 'episode*'))
		episode_paths.sort(key=alphanumeric_key)
		
		# foreach episode paths
		for episode_path in episode_paths:
		
			# get ordered part paths
			part_paths = glob.glob(os.path.join(episode_path, 'part*'))
			part_paths.sort(key=alphanumeric_key)
			
			# foreach parts
			for part_path in part_paths:
				
				# get sorted measurements
				cam_center_paths = glob.glob(os.path.join(part_path,'CameraCentral*'))
				cam_center_paths.sort(key=alphanumeric_key)
				
				st_end_pos = {}
				
				# if situation junction or change lane
				if self._subtype == "junction" or self._subtype == "change_lane":
					
					# get start and end record ids
					start_id = cam_center_paths[0].split('_')[-1].split('.')[0]
					end_id = cam_center_paths[-1].split('_')[-1].split('.')[0]
					
					# get positions
					st_end_pos = self._get_start_end_positions(part_path, start_id, end_id)
				
				# foreach measurements
				for cam_center_path in cam_center_paths:
					
					# get measurement identifier from path
					record_id = cam_center_path.split('_')[-1].split('.')[0]
					
					# paths to data files
					cam_right_path = os.path.join(part_path, "CameraRight_" + record_id + ".png")
					cam_left_path = os.path.join(part_path, "CameraLeft_" + record_id + ".png")
					other_path = os.path.join(part_path, "OtherSensors_" + record_id + ".npy")
					
					# check if all files exists - if not skip measurement
					if not os.path.exists(cam_center_path) or not os.path.exists(cam_right_path) or not os.path.exists(cam_left_path) or not os.path.exists(other_path):
						continue
					
					# append paths
					c_cam_paths.append(cam_center_path)
					l_cam_paths.append(cam_left_path)
					r_cam_paths.append(cam_right_path)
					other_paths.append(other_path)
					start_end_positions.append(st_end_pos)
					
		# save preloads
		save_path = os.path.join(self._directory, "preloads" , self._subtype + "_preload.npy")
		np.save(save_path, [c_cam_paths, l_cam_paths, r_cam_paths, other_paths, start_end_positions])
					
		return c_cam_paths, l_cam_paths, r_cam_paths, other_paths, start_end_positions
	
	def _get_start_end_positions(self, part_path, first_id, last_id):
		"""Get start and end positions from part when in junction or changing lane.
		
		Parameters
		----------
		part_path : string
			Path to part.
		first_id : string
			String id with number of first measurement.
		last_id : string
			String id with number of last measurement.
			
		Returns
		-------
		data : dict
			Dict with start and end position.
		
		"""
		
		# get file paths
		first_file = os.path.join(part_path, "OtherSensors_" + first_id + ".npy")
		last_file = os.path.join(part_path, "OtherSensors_" + last_id + ".npy")
		
		# load data
		first_data = np.load(first_file, allow_pickle=True).item(0)
		last_data = np.load(last_file, allow_pickle=True).item(0)
		
		# create data for return
		data = {
			'start_position': first_data['gps'],
			'end_position': last_data['gps']
		}
		
		return data
