#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of dataset.
#
# It contains functions to select datasets. 
#
# Author:	Marek Dopita (xdopit03)
#

from .xdopit03_dataset_loader import Xdopit03DatasetLoader
from utilities import UndefinedException

def Dataset(directory, dataset_type_name):
	"""Return handler for different datasets.
	
	Parameters
	----------
	directory : str
		Path to dataset directory.
	dataset_type_name : str
		Name of dataset type.
	
	Raises
	------
	UndefinedException
		If dataset type name does not exist.
	
	Returns
	-------
	dataset
		Class for hadling dataset.
	"""
	
	if dataset_type_name == "xdopit03_d_cl":
		return Xdopit03DatasetLoader(directory, 'change_lane')
	elif dataset_type_name == "xdopit03_d_ju":
		return Xdopit03DatasetLoader(directory, 'junction')
	elif dataset_type_name == "xdopit03_d_lf":
		return Xdopit03DatasetLoader(directory, 'lane_follow')
	else:
		raise UndefinedException('Udefined dataset name "' + dataset_type_name + '".\nPossible dataset types: xdopit03_d_cl, xdopit03_d_ju, xdopit03_d_lf')
