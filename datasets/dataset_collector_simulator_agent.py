#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of datasets.
#
# It contains autonomous dataset collector agent class for leaderboard. 
#
# Author:	Marek Dopita (xdopit03)
#

from agents.navigation.local_planner import RoadOption
from agents.navigation.basic_agent import BasicAgent
from agents.tools.misc import compute_distance, is_within_distance, is_within_distance_ahead
from agents.navigation.agent import AgentState

class DatasetCollectorSimulatorAgent(BasicAgent):
	"""Implements extension on carla simulator basic agent. Better collision system, stop at stop signs, avoid pedestrians.
	
	Attributes
	----------
	_stop : 
		Counter for number of steps to keep at stop sign.
	
	Methods
	-------
	run_step(debug)
		One step of navigation.
	_is_vehicle_hazard_pedestrians(vehicle_list, ego_vehicle_location, ego_vehicle_waypoint, proximity_th, up_angle_th, low_angle_th):
		If vehicle in hazard with pedestrian.
	_is_vehicle_hazard_obstacle(vehicle_list, ego_vehicle_location, ego_vehicle_waypoint)
		If vehicle in hazard with obstacle.
	get_incoming_waypoint_and_direction(steps)
		Get waypoint and direction steps ahead.
	"""
	
	def __init__(self, vehicle):
		"""Class initialization.
		
		Parameters
		----------
		vehicle : carla.Vehicle
			Vehicle to drive.
		"""
		
		# init basic agent
		super(DatasetCollectorSimulatorAgent, self).__init__(vehicle)
		
		# set stop steps to zero
		self._stop = 0
		
	def run_step(self, debug=False):
		"""Execute one step of navigation.
		
		Parameters
		----------
		debug : bool
			If debug prints enabled.
		
		Returns
		-------
		control : carla.VehicleControl
			Control for vehicle.
		"""
		
		# is there an obstacle
		hazard_detected = False
		
		# get lists of actors
		actor_list = self._world.get_actors()
		vehicle_list = actor_list.filter("*vehicle*")
		lights_list = actor_list.filter("*traffic_light*")
		pedestrians_list = actor_list.filter("*walker.pedestrian*")
		
		# location of self vehicle
		ego_vehicle_loc = self._vehicle.get_location()
		ego_vehicle_wp = self._map.get_waypoint(ego_vehicle_loc)
		
		# get stop signs
		signs = ego_vehicle_wp.get_landmarks_of_type(1, '206')
		
		# if at stop sign set stop steps
		if len(signs) > 0 and self._stop == 0:
			if str(signs[0].orientation) == "Positive":
				self._stop = 100
		
		# if at stop sign wait
		if self._stop != 0:
			self._stop -= 1
			self._state = AgentState.BLOCKED_RED_LIGHT
			hazard_detected = True
		
		# get pedestrian hazard
		pedestrian_state, pedestrian, distance = self._is_vehicle_hazard_pedestrians(pedestrians_list, ego_vehicle_loc, ego_vehicle_wp)
		
		# if pedestrian ahead
		if pedestrian_state:
			self._state = AgentState.BLOCKED_BY_VEHICLE
			hazard_detected = True
		
		# check possible obstacles
		vehicle_state, vehicle = self._is_vehicle_hazard_obstacle(vehicle_list, ego_vehicle_loc, ego_vehicle_wp)
		
		# if vehicle ahead
		if vehicle_state:
			self._state = AgentState.BLOCKED_BY_VEHICLE
			hazard_detected = True
		
		# check for the state of the traffic lights
		light_state, traffic_light = self._is_light_red(lights_list)
		
		# if at red light
		if light_state:
			self._state = AgentState.BLOCKED_RED_LIGHT
			hazard_detected = True
		
		# if hazard ahead
		if hazard_detected:
			control = self.emergency_stop()
		else:
			self._state = AgentState.NAVIGATING
			control = self._local_planner.run_step(debug=debug)
		
		return control
		
	def _is_vehicle_hazard_pedestrians(self, vehicle_list, ego_vehicle_location, ego_vehicle_waypoint, proximity_th = 10, up_angle_th = 20, low_angle_th=0):
		"""If vehicle is in hazard with pedestrians.
		
		Parameters
		----------
		vehicle_list : list
			List of vehicles in world.
		ego_vehicle_location : carla.Transform
			Location of vehicle.
		ego_vehicle_waypoint : carla.Waypoint
			Waypoint of vehicle.
		proximity_th : int
			Treshold distance where to detect obstacles.
		up_angle_th : int
			Maximal horizontal angle ahead of car to check.
		low_angle_th : int
			Minimal horizontal angle ahead of car to check.
			
		Returns
		-------
		tuple(bool, carla.Actor)
			Tuple if vehicle is blocked and id with blocking actor.
		"""
		
		# foreach actors
		for target_vehicle in vehicle_list:
		
			# ship ego actor
			if target_vehicle.id == self._vehicle.id:
				continue
			
			# if the object is not in our lane it's not an obstacle
			target_vehicle_waypoint = self._map.get_waypoint(target_vehicle.get_location())
			if target_vehicle_waypoint.road_id != ego_vehicle_waypoint.road_id or \
				target_vehicle_waypoint.lane_id != ego_vehicle_waypoint.lane_id:
				continue
			
			# if is pedestrian ahead
			if is_within_distance(target_vehicle.get_location(), ego_vehicle_location,
								self._vehicle.get_transform().rotation.yaw,
								proximity_th, up_angle_th, low_angle_th):
				return (True, target_vehicle, compute_distance(target_vehicle.get_location(), ego_vehicle_location))
				
		return (False, None, -1)
		
	def _is_vehicle_hazard_obstacle(self, vehicle_list, ego_vehicle_location, ego_vehicle_waypoint):
		"""If vehicle has obstacle ahead.
		
		Parameters
		----------
		vehicle_list : list
			List of vehicles in world.
		ego_vehicle_location : carla.Transform
			Location of ego vehicle.
		ego_vehicle_waypoint : carla.Waypoint
			Waypoint where ego vehicle is.
		
		Returns
		-------
		tuple(bool, carla.Vehicle)
			Tuple if vehicle is blocked and id with blocking vehicle.
		"""
		
		# list of waypoints ahead
		waypoints = [ego_vehicle_waypoint]
		for i in range(0,10):
			waypoints.append(self.get_incoming_waypoint_and_direction(steps=i)[0])
		
		# foreach vehicle
		for target_vehicle in vehicle_list:
			
			# do not account for the ego vehicle
			if target_vehicle.id == self._vehicle.id:
				continue
			
			# get waypoint of target vehicle
			target_vehicle_waypoint = self._map.get_waypoint(target_vehicle.get_location())
			
			# set vehicle not in range
			in_range = False
			
			# foreach waypoints
			for wp in waypoints:
				
				# if waypoint is none
				if wp is None:
					continue
				
				# if target car near route 
				if (target_vehicle_waypoint.road_id == wp.road_id or \
					target_vehicle_waypoint.road_id -1 == wp.road_id or target_vehicle_waypoint.road_id +1 == wp.road_id) \
					and target_vehicle_waypoint.lane_id == wp.lane_id:
					
					# set in range
					in_range = True
					break
			
			# if not in range skip car
			if not in_range:
				continue
			
			# if is ahead of vehicle
			if is_within_distance_ahead(target_vehicle.get_transform(),
										self._vehicle.get_transform(),
										self._proximity_vehicle_threshold):
				return (True, target_vehicle)
				
		return (False, None)
		
	def get_incoming_waypoint_and_direction(self, steps=3):
		"""Returns direction and waypoint at a distance ahead defined by the user.
		
		Parameters
		----------
		steps : int
			Number of steps to get the incoming waypoint.
		
		Returns
		-------
		waypoint : carla.Waypoint
			Waypoint.
		road_option : RoadOption
			Road option of waypoint.
		"""
		
		# return waypoint
		if len(self._local_planner._waypoints_queue) > steps:
			return self._local_planner._waypoints_queue[steps]
		else:
			try:
				wpt, direction = self._local_planner._waypoints_queue[-1]
				return wpt, direction
			except IndexError as i:
				return None, RoadOption.VOID
		return None, RoadOption.VOID
