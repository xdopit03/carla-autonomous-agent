#!/usr/bin/env python3

#
# Project:	Bachelor thesis - Neural networks for autonomous driving.
#
# This file is part of datasets.
#
# It contains autonomous dataset collector agent class for leaderboard. 
#
# Author:	Marek Dopita (xdopit03)
#

import carla
import glob
import os
import numpy as np
import cv2

from datasets.dataset_collector_simulator_agent import DatasetCollectorSimulatorAgent
from srunner.scenariomanager.carla_data_provider import CarlaDataProvider
from leaderboard.autoagents.autonomous_agent import AutonomousAgent, Track
from utilities import alphanumeric_key

def get_entry_point():
	"""Getter for agent class name.
	
	Returns
	-------
	str
		Agent class name
	"""
	
	return 'DatasetCollectorAgent'
	
class DatasetCollectorAgent(AutonomousAgent):
	"""Autonomous agent class.
	
	Attributes
	----------
	_route_assigned : bool
		Informs if route plan was assign.
	_data_path : str
		Path to dataset directory.
	_new_episode_id : str
		Id for new episode.
	_agent : DatasetCollectorSimulatorAgent
		Agent behavior object.
	_record : int
		Id of next record.
	
	Methods
	-------
	setup(path_to_conf_file)
		Setup autonomous agent.
	sensors()
		Setup sensors on vehicle.
	run_step(input_data, timestamp)
		Create one step of simulation.
	_collect_data(input_data, timestamp, control)
		Collect data from control and sensors.
	_init_data_collection()
		Initialize repository with new episodes.
	_init_agent()
		Init behavior agent.
	_assign_route()
		Assign route plan to behavior agent.
	"""
	
	def setup(self, path_to_conf_file):
		"""Initializations and definitions for agent.
		
		Parameters
		----------
		path_to_conf_file : str
			Path to configuration file.
		"""
		
		# set sensor track
		self.track = Track.SENSORS
		
		# if route was assigned from global plan
		self._route_assigned = False
		
		# remember dataset path
		self._data_path = path_to_conf_file
		
		# new id
		self._new_episode_id = "00000"
		
		# behavior agent
		self._agent = None
		
		# record identifier
		self._record = 0
		
	def sensors(self):
		"""Definitions for agent senzors.
		
		Returns
		-------
		sensors : dict
			Dict of sensor definitions.
		"""
		
		sensors = [
			{'type': 'sensor.camera.rgb','id': 'CameraCentral', 'x': 2.0, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': -15.0, 'yaw': 0.0, 'width': 800, 'height': 600, 'fov': 100},
			{'type': 'sensor.camera.rgb','id': 'CameraLeft', 'x': 2.0, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': -15.0, 'yaw': -30.0, 'width': 800, 'height': 600, 'fov': 100},
			{'type': 'sensor.camera.rgb','id': 'CameraRight', 'x': 2.0, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': -15.0, 'yaw': 30.0, 'width': 800, 'height': 600, 'fov': 100},
			{'type': 'sensor.lidar.ray_cast','id': 'Lidar', 'x': 0.0, 'y': 0.0, 'z': 2.5, 'roll': 0.0, 'pitch': 0.0, 'yaw': 0.0},
			{'type': 'sensor.other.gnss', 'id': 'GPS', 'x': 0.0, 'y': 0.0, 'z': 2.5},
			{'type': 'sensor.other.imu', 'id': 'IMU', 'x': 0.7, 'y': -0.4, 'z': 1.60, 'roll': 0.0, 'pitch': 0.0, 'yaw': -45.0},
			{'type': 'sensor.speedometer', 'id': 'Speed'},
			{'type': 'sensor.other.radar', 'id': 'RADARFront', 'x': 2.0, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': 0.0, 'yaw': 0.0, 'fov': 100},
			{'type': 'sensor.other.radar', 'id': 'RADARBack', 'x': -2.2, 'y': 0.0, 'z': 1.40, 'roll': 0.0, 'pitch': 0.0, 'yaw': 180.0, 'fov': 100},
		]
		
		return sensors
		
	def run_step(self, input_data, timestamp):
		"""Produce new action for carla.VehicleControl object.
		
		Parameters
		----------
		input_data : dict
			Data from sensors.
		timestamp : float
			Timestamp value of simulation.
			
		Returns
		-------
		control : carla.VehicleControl
			Control of vehicle.
		"""
		
		# prepare control
		control = carla.VehicleControl()
		control.steer = 0.0
		control.throttle = 0.0
		control.brake = 0.0
		control.hand_brake = False
		
		# if agent was not created
		if not self._agent:
			
			# init agent
			self._init_agent()
			
			# init data collection
			self._init_data_collection()
			
			return control
		
		# note was not assigned yet
		if not self._route_assigned:
			
			# assign route
			self._assign_route()
			
		else:
			# move one step agent
			control = self._agent.run_step()
			self._collect_data(input_data, timestamp, control)
			
		return control
		
	def _collect_data(self, input_data, timestamp, control):
		"""Collect data from sensors and agent.
		
		Parameters
		----------
		input_data : dict
			Dict with sensors data.
		timestamp : float
			Simulator timestamp.
		control : carla.VehicleControl
			Control of vehicle.
		"""
		
		# crate record id
		record_id = str(self._record).zfill(5)
		
		# increment record
		self._record += 1
		
		# save images from cameras
		cv2.imwrite(os.path.join(self._data_path, 'episode_' + self._new_episode_id, "CameraCentral_" + record_id + ".png"), input_data['CameraCentral'][1])
		cv2.imwrite(os.path.join(self._data_path, 'episode_' + self._new_episode_id, "CameraLeft_" + record_id + ".png"), input_data['CameraLeft'][1])
		cv2.imwrite(os.path.join(self._data_path, 'episode_' + self._new_episode_id, "CameraRight_" + record_id + ".png"), input_data['CameraRight'][1])
		
		# create numpy control
		np_control = np.array([control.throttle, control.brake, control.steer])
		
		# sensors data dict
		data = {
			'lidar': input_data['Lidar'][1],
			'radar_front': input_data['RADARFront'][1],
			'radar_back': input_data['RADARBack'][1],
			'gps': input_data['GPS'][1],
			'imu': input_data['IMU'][1],
			'speed': input_data['Speed'][1],
			'control': np_control
		}
		
		# save other sensors data
		np.save(os.path.join(self._data_path, 'episode_' + self._new_episode_id, "OtherSensors_" + record_id + ".npy"), data)
		
	def _init_data_collection(self):
		"""Init data collection.
		"""
		
		# get episodes
		episodes_paths = glob.glob(os.path.join(self._data_path, 'episode*'))
		episodes_paths.sort(key=alphanumeric_key)
		
		# if exists episodes - get new id 
		if not len(episodes_paths) == 0:
			
			# get last id as integer
			last_id = int(episodes_paths[-1].split('_')[-1])
			
			# increment last id
			last_id += 1
			
			# create string id
			self._new_episode_id = str(last_id).zfill(5)
		
		# create episodes
		os.mkdir(os.path.join(self._data_path, 'episode_' + self._new_episode_id))
		
	def _init_agent(self):
		"""Init agent to vehicle.
		"""
		# selected actor 
		actor_selected = None
		
		# foreach actors in world
		for actor in CarlaDataProvider.get_world().get_actors():
			
			# select hero actor
			if 'role_name' in actor.attributes and actor.attributes['role_name'] == 'hero':
				actor_selected = actor
				break
				
		# if actor was selected
		if actor_selected:
			
			# create agent for hero actor
			self._agent = DatasetCollectorSimulatorAgent(actor_selected)
		
	def _assign_route(self):
		"""Assign route plan to agent.
		"""
		# if global plan exists
		if self._global_plan:
			plan = []
			
			# previous position
			prev = None
			
			# foreach world coordinates from plan
			for transform, _ in self._global_plan_world_coord:
				
				# create waypoint from coordinates
				wp = CarlaDataProvider.get_map().get_waypoint(transform.location)
				
				# if previous set create route segment
				if prev:
					route_segment = self._agent._trace_route(prev, wp)
					plan.extend(route_segment)
					
				# set actual point as previous
				prev = wp
			
			# set local planner and update agent
			self._agent._local_planner.set_global_plan(plan)
			self._route_assigned = True
